namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMembershipTypes : DbMigration
    {
        public override void Up()
        {
            this.Sql("INSERT INTO MembershipTypes(Id,SignUpFee,DurationInMonths,DiscountRate) VALUES(1,0,0,0)");
            this.Sql("INSERT INTO MembershipTypes(Id,SignUpFee,DurationInMonths,DiscountRate) VALUES(2,30,1,10)");
            this.Sql("INSERT INTO MembershipTypes(Id,SignUpFee,DurationInMonths,DiscountRate) VALUES(3,90,3,15)");
            this.Sql("INSERT INTO MembershipTypes(Id,SignUpFee,DurationInMonths,DiscountRate) VALUES(4,300,12,20)");
        }
        
        public override void Down()
        {
        }
    }
}
